import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
})
export class MenuComponent implements OnInit {
  pizzaItems: any[];
  pastaItems: any[];
  startersItems: any[];
  menus = [
    {
      id: 1,
      food_name: 'Chicken ',
      food_categories: 'pizza',
      items: [
        { name: 'Fresh Tomatoes', price: 260 },
        { name: 'mozzarella', price: 300 },
        { name: 'chicken', price: 400 },
        { name: 'onions', price: 200 },
      ],
      is_best_deal: true,
      has_discount: true,
      discount_in_percentage: 5,
    },

    {
      id: 2,
      food_name: 'Formaggio',
      food_categories: 'pizza',
      items: [
        { name: 'mozzarella cheeses', price: 560 },
        { name: 'parmesan Cheese', price: 400 },
        { name: 'pecorino Cheese', price: 420 },
        { name: 'jarlsberg Cheese', price: 500 },
      ],
      is_best_deal: false,
    },
    {
      id: 2,
      food_name: 'Thai Soup',
      food_categories: 'starters',
      items: [
        { name: 'shrimp', price: 560 },
        { name: 'lemongrass', price: 400 },
        { name: 'mushroom', price: 420 },
      ],
      is_best_deal: true,
      has_discount: true,
      discount_in_percentage: 2,
    },
    {
      id: 3,
      food_name: 'Margherita',
      food_categories: 'pizza',
      items: [
        { name: 'Fresh Tomatoes', price: 500 },
        { name: 'Fresh Mozzarella', price: 1000 },
        { name: 'Fresh Basil', price: 1300 },
      ],
      is_best_deal: true,
    },
    {
      id: 4,
      food_name: 'Lemon Ricotta ',
      food_categories: 'pasta',
      items: [
        { name: 'Special sauce', price: 260 },
        { name: 'mozzarella', price: 800 },
        { name: 'parmesan', price: 1000 },
        { name: 'ground beef', price: 300 },
      ],
    },
    {
      id: 4,
      food_name: "Pineapple'o'clock ",
      food_categories: 'pizza',
      items: [
        { name: 'Fresh Tomatoes', price: 560 },
        { name: 'mozzarella', price: 500 },
        { name: 'fresh pineapple', price: 1300 },
        { name: 'bacon', price: 1300 },
        { name: 'fresh basil', price: 300 },
      ],
      is_best_deal: true,
    },
    {
      id: 4,
      food_name: 'Garlic bread',
      food_categories: 'starters',
      items: [
        { name: 'Grilled ciabatta', price: 260 },
        { name: 'garlic butter', price: 340 },
        { name: 'onions', price: 200 },
      ],
      is_best_deal: true,
    },
    {
      id: 4,
      food_name: 'Lasagna ',
      food_categories: 'pasta',
      items: [
        { name: 'Special sauce', price: 560 },
        { name: 'mozzarella', price: 1000 },
        { name: 'parmesan', price: 1300 },
        { name: 'ground beef', price: 1300 },
      ],
      is_best_deal: true,
      has_discount: true,
      discount_in_percentage: 3,
    },
    {
      id: 4,
      food_name: 'Bruschetta',
      food_categories: 'starters',
      items: [
        { name: 'Bread with pesto', price: 560 },
        { name: 'tomatoes', price: 1000 },
        { name: 'onion', price: 300 },
        { name: 'garlic', price: 1300 },
      ],
      is_best_deal: false,
    },
  ];

  constructor() {}

  ngOnInit(): void {
    this.pizzaItems = this.menus.filter(
      (item) => item.food_categories.toLowerCase() === 'pizza'
    );
    this.pastaItems = this.menus.filter(
      (item) => item.food_categories.toLowerCase() === 'pasta'
    );
    this.startersItems = this.menus.filter(
      (item) => item.food_categories.toLowerCase() === 'starters'
    );
  }

  totalPrice(menu) {
    let items = menu.items;
    let totalPrice = 0;
    let discountedPrice;
    items.forEach((item) => (totalPrice += item.price));
    if (menu.has_discount) {
      discountedPrice = Math.round(
        totalPrice - totalPrice * (menu.discount_in_percentage / 100)
      );
      return { totalPrice, discountedPrice };
    } else {
      discountedPrice = totalPrice;
      return { discountedPrice };
    }
  }

  onTop() {
    {
      let scrollStep = -window.scrollY / (300 / 15),
        scrollInterval = setInterval(() => {
          if (window.scrollY != 0) {
            window.scrollBy(0, scrollStep);
          } else {
            clearInterval(scrollInterval);
          }
        }, 15);
    }
  }
}
