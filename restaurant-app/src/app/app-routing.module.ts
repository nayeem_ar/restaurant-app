import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./main/pages/home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./main/pages/home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'menu',
    loadChildren: () =>
      import('./main/pages/menu/menu.module').then((m) => m.MenuModule),
  },
  {
    path: 'about',
    loadChildren: () =>
      import('./main/pages/about/about.module').then((m) => m.AboutModule),
  },
];

@NgModule({
  imports: [BrowserModule, RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
